package ru.tsc.ichaplygina.taskmanager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;

@UtilityClass
public final class ValidationUtil {

    public static boolean isEmptyString(@Nullable final String string) {
        return string == null || string.isEmpty();
    }

}
