package ru.tsc.ichaplygina.taskmanager.api.entity;

public interface IWBS extends IHasCreated, IHasDateFinish, IHasDateStart, IHasName, IHasStatus {

}
